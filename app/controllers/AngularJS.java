package controllers;

import play.mvc.*;


public class AngularJS extends Controller {

	  public static Result template(String templateName) {
			return ok(new java.io.File("public/app/partials/" + templateName));
	  }

}