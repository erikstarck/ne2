package controllers;


import org.codehaus.jackson.node.ObjectNode;

import play.api.templates.Html;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.libs.WS;
import play.mvc.*;
import play.Logger;

import views.html.*;

public class Application extends Controller {
  
	  public static class Login {
	        
	        public String email;
	        public String password;
	        
	        public String validate() {
	        	//Call web service
	        	String sessionId = controllers.services.JsonFetcher.login(email, password);
	    		session("JSESSIONID", sessionId);
	            return null;
	        }
	        
	    }

	  	/**
	     * Login page.
	     */
	    public static Result login() {
	        return ok(login.render(form(Login.class)));
	    }
	    
	  
	  public static class Search {
		  public String searchterm;
	  }

	  public static Result facetSearch() {
		  return ok(views.html.norstedts.norstedtsfacet.render());
	  }

	  
	  public static Result search() {
			return ok(search.render(form(Search.class)));
	  }
	  
      public static Result autocompleteSearchAsJson(String word, String translateForm, String translateTo) {
			  //ObjectNode result = Json.newObject();
			String lexikon = "svensk-engelsk-stora";
			  F.Promise<WS.Response> jpromise_search=null;
			if("Engelska".equals(translateForm)) {
				lexikon = "engelsk-svensk-stora";
			}

		  	jpromise_search = (WS.url("http://apoc.neint.se:8080/solr/select")
			   .setQueryParameter("q", "strict:" + word + "*")
//					   .setQueryParameter("q", "*:*")
//					   .setQueryParameter("facet.prefix", word)
//					   .setQueryParameter("fq", "type:norstedts AND subtype:svensk-fransk-stora")
					   .setQueryParameter("fq", "type:norstedts AND subtype:" + lexikon)
					   .setQueryParameter("facet", "true")
					   .setQueryParameter("facet.limit", "10")
					   .setQueryParameter("facet.field", "strict")
					   .setQueryParameter("rows", "0")
					   .setQueryParameter("wt", "json")
					   .setHeader("Accept", "application/json")
					   .setHeader("Accept-Charset", "ISO-8859-1,utf-8")
					   .setHeader("Cookie","JSESSIONID="+session("JSESSIONID"))
					   .get());


		  	String body = jpromise_search.get().getBody();
			org.codehaus.jackson.JsonNode result = Json.parse(body);

			Logger.info("Body: " + body);
			
		    return ok(result);
		}
	
	
	  public static Result translateWord( String word, String translateForm, String translateTo) {
			  //ObjectNode result = Json.newObject();
			String lexikon = "svensk-engelsk-stora";
			  F.Promise<WS.Response> jpromise_search=null;
			if("Engelska".equals(translateForm)) {
				lexikon = "engelsk-svensk-stora";
			}
			  	jpromise_search = (WS.url("http://apoc.neint.se:8080/solr/select")
	//		  	jpromise_search = (WS.url("http://api.ne.se/search")
						   .setQueryParameter("q", word)
						   .setQueryParameter("fq", "type:norstedts AND subtype:" + lexikon)
						   .setQueryParameter("rows", "20")
						   .setQueryParameter("wt", "json")
						   .setHeader("Accept", "application/json")
						   .setHeader("Accept-Charset", "ISO-8859-1,utf-8")
						   .setHeader("Cookie","JSESSIONID="+session("JSESSIONID"))
						   .get());


			  	String body = jpromise_search.get().getBody();
				org.codehaus.jackson.JsonNode result = Json.parse(body);

				Logger.info("Body: " + body);

			    return ok(result);
	}
	
	  public static Result facetSearchAsJson(String word) {
		  //ObjectNode result = Json.newObject();
		  
		  F.Promise<WS.Response> jpromise_search=null;

		  	jpromise_search = (WS.url("http://apoc.neint.se:8080/solr/select")
//		  	jpromise_search = (WS.url("http://api.ne.se/search")
					   .setQueryParameter("q", word)
					   .setQueryParameter("fq", "type:encyklopedi OR type:norstedts OR type:årsbok OR type:ur OR type:ordbok OR type:bild")
					   .setQueryParameter("facet", "true")
					   .setQueryParameter("facet.field", "type")
					   .setQueryParameter("rows", "60")
					   .setQueryParameter("fl", "title,summary,type,asset_path")
					   .setQueryParameter("wt", "json")
					   .setHeader("Accept", "application/json")
					   .setHeader("Accept-Charset", "ISO-8859-1,utf-8")
					   .setHeader("Cookie","JSESSIONID="+session("JSESSIONID"))
					   .get());
		  
		  	
		  	String body = jpromise_search.get().getBody();
			org.codehaus.jackson.JsonNode result = Json.parse(body);

			Logger.info("Body: " + body);

		    return ok(result);
	  }
	  
	  
	  public static Result searchAsJson(String word) {
		  //ObjectNode result = Json.newObject();
		  
		  F.Promise<WS.Response> jpromise_search=null;

//			jpromise_search = (WS.url("http://localhost:8081/search")
		  	jpromise_search = (WS.url("http://api.ne.se/search")
					   .setQueryParameter("q", word)
//					   .setQueryParameter("fq", "type:norstedts AND subtype:svensk-fransk-stora")
					   .setQueryParameter("fq", "type:encyklopedi")
					   .setQueryParameter("rows", "10")
					   .setHeader("Accept", "application/json")
					   .setHeader("Accept-Charset", "ISO-8859-1,utf-8")
					   .setHeader("Cookie","JSESSIONID="+session("JSESSIONID"))
					   .get());
		  
		  	String body = jpromise_search.get().getBody();
			org.codehaus.jackson.JsonNode result = Json.parse(body);
		  
		    return ok(result);
	  }
	  
	  public static Result dosearch() {
		  //Show search result
		  Form<Search> searchForm = form(Search.class).bindFromRequest();
	  	
	
  		F.Promise<WS.Response> jpromise_search=null;

			jpromise_search = (WS.url("http://localhost:8081/search")
					   .setQueryParameter("q", searchForm.get().searchterm)
					   .setQueryParameter("fq", "type:encyklopedi")
					   .setQueryParameter("rows", "10")
					   .setHeader("Accept", "text/html")
					   .setHeader("Accept-Charset", "ISO-8859-1,utf-8")
					   .setHeader("Cookie","JSESSIONID="+session("JSESSIONID"))
					   .get());
		
		  
			return ok(searchresult.render(jpromise_search.get().getBody(), form(Search.class))) ;
	  }
	
	    
	    /**
	     * Handle login form submission.
	     */
	    public static Result authenticate() {
	        Form<Login> loginForm = form(Login.class).bindFromRequest();
	        if(loginForm.hasErrors()) {
	        
	            return badRequest(login.render(loginForm));
	        } else {
	            session("email", loginForm.get().email);
	            
	            return ok(index.render("Welcome", form(Search.class)));
	        }
	    }
	
  
	    public static Result index() {
	    	return ok(views.html.norstedts.norstedts.render());
	    }
  
}