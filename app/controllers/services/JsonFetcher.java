package controllers.services;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.http.Cookie;
import play.libs.*;
import com.ning.http.client.Realm;

import ch.qos.logback.core.boolex.Matcher;

public class JsonFetcher {
	
	
	/**
	 * Return jsessionid
	 * @param username
	 * @param password
	 * @return
	 */
	public static String login(String username, String password ) 
	{
		String postThis = "j_username=" + username +  "&j_password=" +password;
		String jSessionId = null;
//		F.Promise<WS.Response> jpromise = WS.url("http://localhost:8081/auth/login")
		F.Promise<WS.Response> jpromise = WS.url("http://api.ne.se:8081/auth/login")
				   .setHeader("Accept", "application/json")
				   .setHeader("Content-Type","application/x-www-form-urlencoded")
				   .post(postThis);

		//Example:
		//JSESSIONID=D04F204CA5C5A3FD0E5B13601449F37F; Path=/
		String cookieString=jpromise.get().getHeader("Set-Cookie");
		String[] cookies = cookieString.split(";");
		for(int i =0; i<cookies.length;i++) {
			if(cookies[i].startsWith("JSESSIONID")  ) {
				//JSESSIONID=D04F204CA5C5A3FD0E5B13601449F37F
				String[] jSessionCookie = cookies[i].split("=");
				jSessionId = jSessionCookie[1];
			}
		}
		
		//jpromise.get().
		return jSessionId;
	}
	
	
	
	public static org.codehaus.jackson.JsonNode readJsonFromUrl(String u, String sessionId, String encoding) {
		
		URL url = null;
		HttpURLConnection httpConn = null;
		String allLines = "";
		String inputLine = "";

		try {
			url = new URL(u);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		try {
			httpConn = (HttpURLConnection) url.openConnection();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//session
		if(sessionId != null) {
			//Get cookie if already set
			String cookie = httpConn.getRequestProperty(sessionId);
            if (cookie == null) {
                cookie = "";
            }
            if (cookie.length() > 0) {
                cookie += ";";
            }
			httpConn.setRequestProperty("Cookie", cookie + "JSESSIONID=" + sessionId);			
		}
		
		//encoding
		if(encoding != null) {
			if(encoding == "UTF8") {
				httpConn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
			}
		}
        		
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), encoding));
			while ((inputLine = in.readLine()) != null) {
				allLines += inputLine;
			}
			in.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		org.codehaus.jackson.JsonNode results = play.libs.Json.parse(allLines);
		return results;
	}
}
