package models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

/**
 * User entity managed by Ebean
 */
@Entity 
@Table(name="user")
public class User extends Model {

   
	private static final long serialVersionUID = 4893977384325524472L;

	@Id
    @Constraints.Required
    @Formats.NonEmpty
    public Integer id;
    
    @Constraints.Required
    public String name;
    
    @Constraints.Required
    public String password;
    
    public String jsessionid;
    
    // -- Queries
    
    public static Model.Finder<String,User> find = new Model.Finder(String.class, User.class);
    
    /**
     * Retrieve all users.
     */
    public static List<User> findAll() {
        return find.all();
    }

    /**
     * Retrieve a User from email.
     */
    public static User findById(Integer id) {
    	User u = find.where().eq("id", id).findUnique();
        return u;
    }
    
    /**
     * Authenticate a User.
     */
    public static User authenticate(String name, String password) {
        return find.where()
            .eq("name", name)
            .eq("password", password)
            .findUnique();
    }

  
   
}

