import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "ne"
    val appVersion      = "1.0-SNAPSHOT"

  
// Only compile the bootstrap bootstrap.less file and any other *.less file in the stylesheets directory 
def customLessEntryPoints(base: File): PathFinder = ( 
    (base / "app" / "assets" / "stylesheets" / "ne" * "master.less") +++
    (base / "app" / "assets" / "stylesheets"  * "bootstrap.less") +++
    (base / "app" / "assets" / "stylesheets"  * "responsive.less") 
)

val appDependencies = Seq(
  "mysql" % "mysql-connector-java" % "5.1.18",
   "commons-io" % "commons-io" % "1.3.2"
)

    val main = PlayProject(appName, appVersion, appDependencies, mainLang = JAVA).settings(
      	// Add your own project settings here      
		// lessEntryPoints <<= baseDirectory(_ / "app" / "assets" / "stylesheets" ** "bootstrap.less")
		lessEntryPoints <<= baseDirectory(customLessEntryPoints)		
    )

}
