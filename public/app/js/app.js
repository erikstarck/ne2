'use strict';

/* App Module */
var norstedtsApp = angular.module('norstedtsApp', ['ui']); //'$strap.directives'


norstedtsApp.run(function($rootScope) {
	$rootScope.$on('handleTranslate', function(event, args) {
        $rootScope.$broadcast('handleTranslateBroadcast', args);
    });	
});

norstedtsApp.config(function($routeProvider) {
   	$routeProvider.when( "/", {
		templateUrl: "/template/angular/index.html"
	});
   	$routeProvider.when( "/:searchtype/:searchterm", {
		templateUrl: "/template/angular/norstedtsresult.html",
		controller: "NorstedtsIndexController"
	});
  /* 	$routeProvider.when( "/search/:searchterm", {
		templateUrl: "/template/angular/searchresult.html",
		controller: "EverythingSearchIndexController"
	});*/
    $routeProvider.otherwise({ redirectTo: '/search' });
});