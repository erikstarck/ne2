'use strict';

/* Controllers */
norstedtsApp.controller('EverythingSearchResultController', 
	function EverythingSearchResultController($scope, everythingSearchService) {
		$scope.searchTerm 		= everythingSearchService.searchTerm();
		$scope.searchresult 	= everythingSearchService.searchResult();
		$scope.images 			= everythingSearchService.searchResultImages();
	}
);

norstedtsApp.controller('SearchController', 
	function SearchController($scope, $routeParams) {
		$scope.searchBoxes = "/template/angular/norstedtssearchbox.html";
	}
);




norstedtsApp.controller('NorstedtsIndexController', 
	function NorstedtsIndexController($scope, $routeParams, translateService) {
		if($routeParams.searchtype == "search") {
			$scope.$parent.searchBoxes = "/template/angular/searchboxes.html";
		}
		
		if($routeParams.searchterm != undefined) {
			$scope.wordtotranslate = $routeParams.searchterm;
			translateService
				.doTranslate($scope.wordtotranslate, $scope.fromLanguage, $scope.toLanguage)
				.then( function(response) {
					$scope.$emit('handleTranslate');
			});		
		}
	}
);



norstedtsApp.controller('EverythingSearchIndexController', 
	function EverythingSearchIndexController($scope, $http, $location, everythingSearchService) {
		$scope.searchBoxes = "/template/angular/searchboxes.html";
		$scope.$watch("typeaheadkeyword", function(value) {
			if( value != undefined) {
				$location.path("/search/"+value);
			}
	});
});

function NorstedtsController($scope, $http, $location, translateService, $rootScope, $routeParams) {
	$scope.toLanguage = "Engelska";
	$scope.fromLanguage = "Svenska";

	/* Make sure text field is prefilled if parameter is in URL */
	$scope.$on('handleTranslateBroadcast', function(event, args) {
       	$scope.wordtotranslate 	= translateService.searchTerm();
	})

	/* The button. Remove? */
	$scope.translate = function() {
		translateService
		.doTranslate($scope.wordtotranslate, $scope.fromLanguage, $scope.toLanguage)
		.then( function(response) {
			$scope.$emit('handleTranslate');
		});
	};

	/* Change language and make another search with new setting */
	$scope.switchLanguage = function() {
		var tempLanguage = $scope.toLanguage;
		$scope.toLanguage = $scope.fromLanguage;
		$scope.fromLanguage = tempLanguage;
		if($scope.wordtotranslate != null) {
			translateService
				.doTranslate($scope.wordtotranslate, $scope.fromLanguage, $scope.toLanguage)
				.then( function(response) {
					$scope.$emit('handleTranslate');
			});
			
		}
	};

	/* Watch the search field and update the URL on changes. New URL will trigger a new search */
	$scope.$watch("wordtotranslate", function(value) {
		if(value != undefined) {
			$location.path("/norstedts/"+value);
		}
	});
};


norstedtsApp.controller('NorstedtsResultController', 
	function NorstedtsResultController($scope, translateService) {
		$scope.$on('handleTranslateBroadcast', function(event, args) {
        	$scope.searchWord 	= translateService.searchTerm();
			$scope.hitsForWord 	= translateService.hits();
		});
		
		$scope.selectword = function(hit, $event) {
			console.log(hit);
			$event.preventDefault();
			$scope.selectedword = hit;
		}
		
		
	}
	
	
);


	 
