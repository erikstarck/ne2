'use strict';

/* Services */


norstedtsApp.factory('translateService', function($http, $q) {
	var translator = {};
	translator.searchTermValue = '';
	translator.hitsforWord = [];

	translator.doTranslate = function(wordtotranslate, fromLanguage, toLanguage) {
		 var d = $q.defer();
		 $http({
				method: "GET",
				headers: {"Accept": "application/json"},
				url: 'http://localhost:9000/translate?word=' + 
					wordtotranslate + '&translateFrom=' + 
					fromLanguage + '&translateTo=' + toLanguage
			}).success(function(data, status, headers, config) {
				translator.hitsforWord = data.response.docs;
				translator.searchTermValue = wordtotranslate;
				d.resolve();
			}).error(function(data, status, headers, config) {
			    d.reject(status);   
			});
			return d.promise;
	}
	
	
	translator.getAutocompleteWords = function(wordToTranslate, fromLanguage, toLanguage) {
		 var d = $q.defer();
		 var typeahead=[];
	     $http({
				method: "GET",
				headers: {"Accept": "application/json"},
				url: 'http://localhost:9000/autocomplete?query=' + 
					wordToTranslate + '&translateFrom=' + 
					fromLanguage + '&translateTo=' + toLanguage
			}).success(function(data, status, headers, config) {
				for(var i=0; i<data.facet_counts.facet_fields.strict.length; i=i+2) {
					typeahead.push(
						data.facet_counts.facet_fields.strict[i] 
					);
				}
				d.resolve({typeahead: typeahead});
			}).error(function(data, status, headers, config) {
			    d.reject(status);  
			});
			return d.promise;
			
	}

	translator.hits = function() {
		return this.hitsforWord;
	}
	
	translator.searchTerm = function() {
		return this.searchTermValue;
	}
	
	
	return translator;
});


norstedtsApp.service('everythingSearchService', function() {
	var everythingSearcher = {};
	
	everythingSearcher.searchTermValue = '';
	everythingSearcher.searchResultValue = [];
	everythingSearcher.searchResultImagesValue = [];
	
	everythingSearcher.searchResultImages = function() {
		return this.searchResultImagesValue;
	}
	everythingSearcher.setSearchResultImages = function(images) {
		searchResultImagesValue = images;
	}
	

	everythingSearcher.searchResult = function() {
		return this.searchResultValue;
	}
	everythingSearcher.setSearchForResult = function(result) {
		searchResultValue = result;
	}
	

	everythingSearcher.searchFor = function(term) {
		searchTermValue = term;
	}
	everythingSearcher.searchTerm = function() {
		return this.searchTermValue;
	}
	
	everythingSearcher.doSearch = function() {
		 var d = $q.defer();

	        $http({
				method: "GET",
				headers: {"Accept": "application/json"},
				url: 'http://localhost:9000/facetjsonsearch/' + value
			}).success(function(data, status, headers, config) {
				
				var facets = [];
				var images = [];
				
				for(var i=0; i<data.response.docs.length; i++) {
					if(data.response.docs[i].type == "bild") {
						console.log(data.response.docs[i]);
						images.push(data.response.docs[i]);
					}
				}
				
				for(var i=0; i<data.facet_counts.facet_fields.type.length; i=i+2) {
					console.log(data.facet_counts.facet_fields.type[i]);
					
					
					var hitsInFacet = [];
					for(var j=0; j<data.response.docs.length; j++) {
						if(data.response.docs[j].type == data.facet_counts.facet_fields.type[i]) {
							hitsInFacet.push(data.response.docs[j]);
						}
					}
					
					facets.push({
						name : data.facet_counts.facet_fields.type[i],
						size : data.facet_counts.facet_fields.type[i+1],
						hits : hitsInFacet
					});
					
					
				}
				d.resolve();
				everythingSearcher.setSearchResultImages(images);
				everythingSearcher.setSearchForResult(facets);
				
		}).error(function(data, status, headers, config) {
		    $scope.status = status;
		});
	};
	
	
	return everythingSearcher;
});
